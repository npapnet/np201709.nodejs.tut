
- [installation on Windows ](https://nodesource.com/blog/installing-nodejs-tutorial-windows/)

- [Setting up a Node development environment](https://developer.mozilla.org/en-US/docs/Learn/Server-side/Express_Nodejs/development_environment)

- [Setting up Node.js on Windows 10](https://blog.risingstack.com/node-js-windows-10-tutorial/)

# Creating a nodejs module and publish it. 

- [Creating and Publishing a Node.js Module](https://quickleft.com/blog/creating-and-publishing-a-node-js-module/)

- [Exposing your local Node.js app to the world](https://www.pluralsight.com/guides/node-js/exposing-your-local-node-js-app-to-the-world)

## Desktop apps
- [Creating Your First Desktop App With HTML, JS and Node-WebKit](https://tutorialzine.com/2015/01/your-first-node-webkit-app) 

# typescript
-[Editing TypeScript](https://code.visualstudio.com/docs/languages/typescript)